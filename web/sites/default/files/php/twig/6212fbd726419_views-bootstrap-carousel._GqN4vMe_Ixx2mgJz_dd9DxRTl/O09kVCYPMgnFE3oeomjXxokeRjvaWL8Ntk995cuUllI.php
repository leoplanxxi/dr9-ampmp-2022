<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/views_bootstrap/templates/views-bootstrap-carousel.html.twig */
class __TwigTemplate_2d9ee94f655fd889fb8019a06d4ab6a2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "
<div id=\"";
        // line 23
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["id"] ?? null), 23, $this->source), "html", null, true);
        echo "\" class=\"carousel ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["effect"] ?? null), 23, $this->source), "html", null, true);
        echo "\"
    ";
        // line 24
        if (($context["ride"] ?? null)) {
            echo " data-bs-ride=\"carousel\" ";
        }
        // line 25
        echo "    data-bs-pause=\"";
        if (($context["pause"] ?? null)) {
            echo "hover";
        } else {
            echo "false";
        }
        echo "\" ";
        if (($context["keyboard"] ?? null)) {
            echo " data-bs-keyboard=\"true\" ";
        }
        // line 26
        echo ">
  ";
        // line 28
        echo "  ";
        if (($context["indicators"] ?? null)) {
            // line 29
            echo "  <div class=\"carousel-indicators\">
      ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                // line 31
                echo "          ";
                $context["indicator_classes"] = [0 => ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, true, 31)) ? ("active") : (""))];
                // line 32
                echo "          <button type=\"button\" data-bs-target=\"#";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["id"] ?? null), 32, $this->source), "html", null, true);
                echo "\" data-bs-slide-to=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 32, $this->source), "html", null, true);
                echo "\" class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_join_filter($this->sandbox->ensureToStringAllowed(($context["indicator_classes"] ?? null), 32, $this->source), " "), "html", null, true);
                echo "\"></button>
      ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "  </div>
  ";
        }
        // line 36
        echo "
  ";
        // line 38
        echo "  <div class=\"carousel-inner\">
    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 40
            echo "        ";
            $context["row_classes"] = [0 => "carousel-item", 1 => ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, true, 40)) ? ("active") : (""))];
            // line 41
            echo "        <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_join_filter($this->sandbox->ensureToStringAllowed(($context["row_classes"] ?? null), 41, $this->source), " "), "html", null, true);
            echo "\" data-bs-interval=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["interval"] ?? null), 41, $this->source), "html", null, true);
            echo "\">
        ";
            // line 42
            if (twig_get_attribute($this->env, $this->source, $context["row"], "link", [], "any", false, false, true, 42)) {
                // line 43
                echo "        ";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\devel\Twig\Extension\Debug']->kint($this->env, $context, [0 => $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["row"], "link", [], "any", false, false, true, 43), 43, $this->source)]));
                echo " ";
            }
            // line 44
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["row"], "image", [], "any", false, false, true, 44), 44, $this->source), "html", null, true);
            echo "
        ";
            // line 45
            if ((twig_get_attribute($this->env, $this->source, $context["row"], "title", [], "any", false, false, true, 45) || twig_get_attribute($this->env, $this->source, $context["row"], "description", [], "any", false, false, true, 45))) {
                // line 46
                echo "            ";
                if (($context["use_caption"] ?? null)) {
                    // line 47
                    echo "            <div class=\"carousel-caption d-none d-md-block\">
            ";
                }
                // line 49
                echo "            ";
                if (twig_get_attribute($this->env, $this->source, $context["row"], "title", [], "any", false, false, true, 49)) {
                    // line 50
                    echo "                <h3>";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["row"], "title", [], "any", false, false, true, 50), 50, $this->source), "html", null, true);
                    echo "</h3>
            ";
                }
                // line 52
                echo "            ";
                if (twig_get_attribute($this->env, $this->source, $context["row"], "description", [], "any", false, false, true, 52)) {
                    // line 53
                    echo "                <p>";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["row"], "description", [], "any", false, false, true, 53), 53, $this->source), "html", null, true);
                    echo "</p>
            ";
                }
                // line 55
                echo "            ";
                if (($context["use_caption"] ?? null)) {
                    // line 56
                    echo "            </div>
            ";
                }
                // line 58
                echo "        ";
            }
            // line 59
            echo "        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "  </div>
  ";
        // line 63
        echo "  ";
        if (($context["navigation"] ?? null)) {
            // line 64
            echo "    <button class=\"carousel-control-prev\" data-bs-target=\"#";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["id"] ?? null), 64, $this->source), "html", null, true);
            echo "\" type=\"button\" data-bs-slide=\"prev\">
      <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
      ";
            // line 66
            if (($context["prevnextlabel"] ?? null)) {
                // line 67
                echo "      <span class=\"sr-only\">";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Previous"));
                echo "</span>
      ";
            }
            // line 69
            echo "    </button>
    <button class=\"carousel-control-next\" data-bs-target=\"#";
            // line 70
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["id"] ?? null), 70, $this->source), "html", null, true);
            echo "\" type=\"button\" data-bs-slide=\"next\">
      <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
      ";
            // line 72
            if (($context["prevnextlabel"] ?? null)) {
                // line 73
                echo "      <span class=\"sr-only\">";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Next"));
                echo "</span>
      ";
            }
            // line 75
            echo "    </button>
  ";
        }
        // line 77
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/views_bootstrap/templates/views-bootstrap-carousel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 77,  250 => 75,  244 => 73,  242 => 72,  237 => 70,  234 => 69,  228 => 67,  226 => 66,  220 => 64,  217 => 63,  214 => 61,  199 => 59,  196 => 58,  192 => 56,  189 => 55,  183 => 53,  180 => 52,  174 => 50,  171 => 49,  167 => 47,  164 => 46,  162 => 45,  157 => 44,  152 => 43,  150 => 42,  143 => 41,  140 => 40,  123 => 39,  120 => 38,  117 => 36,  113 => 34,  92 => 32,  89 => 31,  72 => 30,  69 => 29,  66 => 28,  63 => 26,  52 => 25,  48 => 24,  42 => 23,  39 => 22,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/views_bootstrap/templates/views-bootstrap-carousel.html.twig", "/var/www/html/buap/ampmp-2022/web/modules/custom/views_bootstrap/templates/views-bootstrap-carousel.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 24, "for" => 30, "set" => 31);
        static $filters = array("escape" => 23, "join" => 32, "t" => 67);
        static $functions = array("kint" => 43);

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for', 'set'],
                ['escape', 'join', 't'],
                ['kint']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
